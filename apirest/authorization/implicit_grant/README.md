# Exemple d'Implicit Grant de Spotify

Aquesta aplicació mostra la informació del teu perfil de Spotify utilitzant [Implicit Grant](https://developer.spotify.com/documentation/web-api/tutorials/implicit-flow)
per concedir permisos a l'aplicació.

> El flux d'Implicit Grant té algunes vulnerabilitats de seguretat significatives, així que desaconsellem encaridament utilitzar aquest flux. Si necessites implementar autorització on no sigui possible emmagatzemar el teu secret de client, utilitza millor Authorization code amb PKCE.

## Instal·lació

Aquest exemple s'executa amb Node.js. Al [seu lloc web](http://www.nodejs.org/download/) pots trobar instruccions sobre com instal·lar-ho.

Instal·la les dependències de l'aplicació amb la següent comanda:

    $ npm install

## Utilitzant les teves pròpies credencials

Hauràs de registrar la teva aplicació i obtenir les teves pròpies credencials des del [Tauler de Desenvolupadors de Spotify](https://developer.spotify.com/dashboard).

- Crea una nova aplicació al tauler i afegeix `http://localhost:8080` a la llista d'URL de redirecció de l'aplicació.
- Un cop hagis creat la teva aplicació, actualitza el `client_id` i `redirect_uri` al fitxer `public/index.html` amb els valors obtinguts de la configuració de l'aplicació al tauler.

## Execució de l'exemple

Des d'una consola:

    $ npm start

Després, obre `http://localhost:8080` en un navegador.

