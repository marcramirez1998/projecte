# Mostra les Dades del teu Perfil de Spotify en una Aplicació Web

Aquest és el codi final per a la Spotify Web API - Com mostrar les Dades del teu Perfil en una Aplicació Web. Pots executar aquesta demo directament o [seguir el tutorial](https://developer.spotify.com/documentation/web-api/howtos/web-app-profile).

## Pre-requisits

Per executar aquesta demo necessitaràs:

- Un entorn [Node.js LTS](https://nodejs.org/en/) o posterior.
- Un Compte de Desenvolupador de [Spotify](https://developer.spotify.com/)

## Ús

Crea una aplicació al teu [Tauler de Desenvolupadors de Spotify](https://developer.spotify.com/dashboard/), estableix la URL de redirecció a `http://localhost:5173/callback` i `http://localhost:5173/callback/`, i copia el teu ID de client.

Clona el repositori, assegura't que et trobes al directori `get_user_profile` i executa:

```bash
npm install
npm run dev

```

Reemplaça el valor de clientId a `/src/script.ts` amb el teu propi ID de client.
